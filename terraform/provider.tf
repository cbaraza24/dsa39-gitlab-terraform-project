terraform { 
  required_providers { 
    aws = { 
      source  = "hashicorp/aws" 
      version = "~> 5.0" 
    } 
  } 
backend "http" { 
}  
} 
# Configure and downloading plugins for aws 
provider "aws" { 
  region     = "${var.aws_region}" 
} 