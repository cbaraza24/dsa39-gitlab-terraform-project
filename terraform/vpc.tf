# Creating VPC 
resource "aws_vpc" "demovpc" { 
  cidr_block       = "${var.vpc_cidr}" // 10.0.0/16
  instance_tenancy = "default" 
  tags = { 
    Name = "DSA39-Demo-VPC" 
  } 
} 